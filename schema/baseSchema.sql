-- Table for Maintain DB Schema Change Log.

CREATE TABLE IF NOT EXISTS `dbChangeLog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(15) UNIQUE NOT NULL COMMENT 'Version  using semver format',
  `comments` varchar(255),
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `INDEX_APPLIED_ON` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `dbChangeLog` (`version`, `comments`) VALUES ('0.0.0', 'Bootstrap project schema');
