#!/bin/bash

if [ "$#" -lt "1" ] || ([ "$1" != "-a" ] && [ "$1" != "-l" ])
then
    echo "Usage: updateDB.sh [params] <env>";
    echo "params - Either one should be provided.";
    echo "    -a   -  Apply new scripts/patches";
    echo "    -l   -  List available new scripts/patches";
    echo "env - Environment name. Default empty with pick db.properties. Make sure db-<env>.properties file is present";
    echo "    prod   -  Uses Prod properties file. db-prod.properties ";
    echo "eg. updateDB.sh -a prod";

    exit 0;
fi

env=$2;

if [ "$env" = "prod" ]
then
. db-$env.properties
else
. db.properties
fi

function log {

    if [ "$params" == "-l" ]
    then
        echo $1;
    elif [ "$params" == "-a" ]
    then
        op=$1;
        echo $op;
        echo $op >> $output_file;
    fi
}

params=$1;
now="$(date)";

DB_CMD_PREFIX="mysql -h$prj_db_host -P$prj_db_port -u$prj_db_user_name -p$prj_db_pwd -D$prj_db_name"

CURR_VER=$($DB_CMD_PREFIX -s -N -e "SELECT version FROM dbChangeLog ORDER BY updated_at DESC, id DESC LIMIT 1") 2>>$error_file;
log "---------------------------------------";
log "Start Execution Time: $now";
log "---------------------------------------";
log "Current Version : $CURR_VER";
log "---------------------------------------";
log "";


OIFS="$IFS";
IFS='.';
read -a sv <<< "$CURR_VER";
IFS="$OIFS";


CURR_MAJOR=${sv[0]};
CURR_MINOR=${sv[1]};
CURR_POINT=${sv[2]};


function validateAndApply {

    filePath=$1;
    fileName=`basename $filePath`;
    version=`expr "${fileName#$prj_script_file_prefix-}"`;
    version=`expr "${version%.sql}"`;

    OIFS="$IFS";
    IFS='.';
    read -a fv <<< "$version";
    IFS="$OIFS";

    re='^[0-9]+$'
    if ! [[ ${fv[0]} =~ $re ]] ; then
       return 0;
    fi

    if [ ${fv[0]} -ge $CURR_MAJOR ] && ([ ${fv[1]} -ge $CURR_MINOR ] || [ ${fv[0]} -gt $CURR_MAJOR ])
    then
        if [ ${fv[2]} -gt $CURR_POINT ] || [ ${fv[1]} -gt $CURR_MINOR ] || [ ${fv[0]} -gt $CURR_MAJOR ]
        then
            if [ "$params" == "-l" ]
            then
                log "$fileName";
                CURR_MAJOR=${fv[0]};
                CURR_MINOR=${fv[1]};
                CURR_POINT=${fv[2]};
            elif [ "$params" == "-a" ]
            then
                ($DB_CMD_PREFIX -s -N -e "source $filePath") >> $output_file 2>>$error_file;
                EXITSTATUS=$?;
                if [ $EXITSTATUS -ne 0 ]
                then
                    log "";
                    log "---------------------------------------";
                    log "Error executing script file: $filePath";
                    log "------------";
                    log "Pathces Applied to version: $CURR_MAJOR.$CURR_MINOR.$CURR_POINT";
                    log "---------------------------------------";
                    exit 1;
                fi
                log "Applied Script/Patch: $fileName";
                CURR_MAJOR=${fv[0]};
                CURR_MINOR=${fv[1]};
                CURR_POINT=${fv[2]};
                ($DB_CMD_PREFIX -s -N -e "INSERT INTO dbChangeLog (version, comments) VALUES ('$CURR_MAJOR.$CURR_MINOR.$CURR_POINT', '$fileName')") >> $output_file 2>>$error_file;
            fi
        fi
    fi
};


if [ "$params" == "-l" ]
then
    log "Available New Scripts/Patches";
    log "---------------------------------------";
fi

files=( $base_schema_path/$prj_script_file_prefix-*.$prj_script_file_suffix );
for i in "${files[@]}"
do
   validateAndApply $i;
done

log "";
log "---------------------------------------";
if [ "$params" == "-l" ]
    then
    log "New Version will be: $CURR_MAJOR.$CURR_MINOR.$CURR_POINT";
elif [ "$params" == "-a" ]
    then
    log "New Version : $CURR_MAJOR.$CURR_MINOR.$CURR_POINT";
fi
log "---------------------------------------";

now="$(date)";
log "---------------------------------------";
log "Completed @: $now";
log "---------------------------------------";
log "";