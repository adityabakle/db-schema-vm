# DB Schema Versioning (with MySql)

This project illustrators a simple approach of database schema version management. It also has the script which can
identify current schema version and apply available/pending versions.
This also provides setting up MySql VM with just one command `vagrant up`. 


### Prerequisites for MySql Virtual Machine (optional)
- Vagrant ([Download](https://www.vagrantup.com/downloads.html))
- Virtual Box ([Download](https://www.virtualbox.org/wiki/Downloads))

### DB Version Table.
`dbChangeLog` is the table used for db schema version management. Its recommended not to modify this table manually.
[Semantic Versioning](http://semver.org) technique is used for managing the schema.
- `version` - Fersion follows the semver format (Ref: [semver](http://semver.org).
- `comments` - Optional Message when applying patch.
- `updated_at` - Timestamp when version was applied.

Version info is saved as string in DB columns.
[baseSchema.sql](schema/baseSchema.sql) - defines the table structure and sets the version to `0.0.0`.
Make sure all your following script files name greater than the base version.

### DB Script/Patch File
It's recommended that every change to DB schema or adding records to table should be done through scripts.
This helps in keeping track of all changes and managing code across environments.
Script file name follows a format. `<prefix>-<MAJOR>.<MINOR>.<POINT>.sql`
example: `script-0.1.0.sql`, `script-0.2.1.sql`

### Configuration
DB configuration and project configuration required are all defined in [db.properties](db.properties) file.
Properties file is self explanatory.

### Update Script
The main file which does all the project management and applying script is `updateDB.sh`
It takes two parameters. Either one should be provided. 
- `-a` - Apply new scripts/patches. (This will actually apply the patches).
- `-l` - List available new scripts/patches. (This will just list whats going to be applied).


### Contact
Any issues/questions or feature request file a bug. I will try to get it.