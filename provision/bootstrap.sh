#!/bin/bash
. /vagrant/db.properties

# Common packages required for VMs to be listed here for installing.
sudo apt-get update --assume-yes
sudo apt-get install --assume-yes ruby curl git


# Installing MySql --- START
# The below lines sets the initial account password which is prompted during installation.

echo mysql-server mysql-server/root_password password $root_pwd | debconf-set-selections
echo mysql-server mysql-server/root_password_again password $root_pwd | debconf-set-selections

# Installs mySQL pkg.
apt-get install --assume-yes mysql-server mysql-client
echo '---------------------> Installed My Sql'
# Installing MySql --- END


# Create Project DB Schema and User with privalages.

mysql -u$root_user_name -p$root_pwd -B -e "CREATE DATABASE $prj_db_name"
mysql -u$root_user_name -p$root_pwd -B -e "CREATE USER '$prj_db_user_name'@'%' IDENTIFIED BY '$prj_db_pwd'"
mysql -u$root_user_name -p$root_pwd -B -e "GRANT ALL ON $prj_db_name.* TO '$prj_db_user_name'@'%'"
mysql -u$root_user_name -p$root_pwd -B -e "FLUSH PRIVILEGES"

# create base schema with DB version Control table.
mysql -u$prj_db_user_name -p$prj_db_pwd -D$prj_db_name -B -e "source /vagrant/$base_schema_path/$base_schema_file"

